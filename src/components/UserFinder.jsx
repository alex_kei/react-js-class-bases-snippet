
import React from 'react';
import Users from './Users';
import UsersContext from '../context/users';
import styles from './UserFinder.module.css';

export default class UserFinder extends React.Component {
  static contextType = UsersContext;

  constructor() {
    super();
    this.state = {
      searchString: '',
      filteredUsers: [],
    };
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange({ target }) {
    this.setState({ searchString: target.value });
  }

  componentDidMount() {
    const filteredUsers = this.context.users;
    this.setState(prev => ({ filteredUsers }));
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevState.searchString === this.state.searchString) {
      return;
    }
    const users = this.context.users;
    const filteredUsers = users.filter(
      user => user.name.includes(this.state.searchString),
    )
    this.setState(prev => ({ filteredUsers }));
  }

  render() {
    const { filteredUsers } = this.state;
    return (
      <>
        <div className={styles.finder}>
          <input type='search' onChange={this.handleChange} />
        </div>
        <Users users={filteredUsers ? filteredUsers : []} />
      </>
    );
  }
};