import React from 'react';
import styles from './User.module.css';

export default class User extends React.Component {
  componentWillUnmount() {
    console.log(`User #${this.props.id} will unmount`);
  }

  render() {
    return <li className={styles.user}>{this.props.name}</li>;
  }
};
