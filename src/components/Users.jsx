import React from 'react';
import User from './User';
import styles from './Users.module.css';

export default class Users extends React.Component {
  state = {
    visible: true,
  };

  toggleUsers = () => {
    this.setState(prev => ({ visible: !prev.visible }));
  };

  render() {
    const { visible } = this.state;
    const usersList = (
      <ul>
        {this.props.users.map(({ id, name }) => (
          <User key={id} name={name} id={id} />
        ))}
      </ul>
    );

    return (
      <div className={styles.users}>
        <button onClick={this.toggleUsers}>
          {visible ? 'Hide' : 'Show'} Users
        </button>
        {visible && usersList}
      </div>
    );
  }
};
