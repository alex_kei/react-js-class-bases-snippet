import UsersContext from './context/users';
import UserFinder from './components/UserFinder';

import getUsers from './data/users';

function App() {
  const usersContext = {
    users: getUsers(),
  }

  return (
    <UsersContext.Provider value={usersContext}>
      <UserFinder />
    </UsersContext.Provider>
  );
}

export default App;
